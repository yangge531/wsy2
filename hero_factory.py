from Police import police
from Timo import timo
class factory:
    def create_hero(self,heroname):
        if heroname=='timo':
            return timo()
        elif heroname=='police':
            return police()
        else:
            print("此英雄不在英雄工厂中")

fa=factory()
ti=fa.create_hero('timo')
po=fa.create_hero('police')
ti.fight(po.power,po.hp)
po.fight(ti.power,ti.hp)
ti.speak_lines(ti.name)
po.speak_lines(po.name)

